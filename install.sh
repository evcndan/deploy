#!/bin/bash

# amzn-ami-hvm-2015.09.0.x86_64-gp2 - ami-e3106686

echo "export LANG=en_US.UTF8" >> ~/.bashrc
source  ~/.bashrc
# To ensure that all of your software packages are up to date, perform a
# quick software update on your instance
# yum update -y
# Now that your instance is updated, you can install the Apache web server,
# MySQL, and PHP software packages.

yum-config-manager --enable epel

yum groupinstall -y "Web Server" "MySQL Database server" "PHP Support"
yum install -y php-mysql

# Start the Apache web server and configure the Apache web server to start
# at each system boot
service httpd start
chkconfig httpd on

# Modify the mode SELinux to permissive mode
yum install -y crudini
setenforce 0
crudini --set /etc/selinux/config '' SELINUX disabled

# Configure the firewall
iptables -I INPUT -i eth0 -p tcp --dport 80 -m state --state NEW,ESTABLISHED -j ACCEPT
# iptables -I INPUT -i eth0 -p tcp --dport 443 -m state --state NEW,ESTABLISHED -j ACCEPT
# Warning: maybe restrict access to localhost only
# iptables -I INPUT -i eth0 -p tcp --dport 9292 -m state --state NEW,ESTABLISHED -j ACCEPT
# iptables --line -vnL
service iptables save
service iptables restart

# To test LAMP web server in a web browser
echo '<p>Hello World!</p>' > /var/www/html/index.html
echo '<?php phpinfo(); ?>' > /var/www/html/phpinfo.php

# Enable request to /~user to serve the user's public_html and
# restart apache web server
sed -i 's/UserDir disabled/UserDir enabled \*/' /etc/httpd/conf/httpd.conf
sed -i 's/#UserDir public_html/UserDir public_html/' /etc/httpd/conf/httpd.conf
service httpd restart

# Add user
adduser sample_app
chmod 0711 /home/sample_app
su -s /bin/bash -c 'mkdir /home/sample_app/public_html' sample_app
chmod +rx /home/sample_app/public_html
su -s /bin/bash -c "echo '<p>Hello World!</p>' >  /home/sample_app/public_html/index.html" sample_app

# Add some extra packages
yum install -y yum-utils patch gcc-c++ readline-devel zlib-devel          \
  libyaml-devel libffi-devel openssl-devel autoconf automake libtool bison \
  sqlite-devel git nodejs

# Postgres
echo '127.0.0.1 db' >> /etc/hosts

if [ -z $(gawk -F= '/^NAME/{print $2}' /etc/os-release | grep 'Amazon Linux AMI') ]; then
  crudini --set /etc/yum.repos.d/CentOS-Base.repo base exclude 'postgresql*'
  crudini --set /etc/yum.repos.d/CentOS-Base.repo updates exclude 'postgresql*'
  cd /tmp
  curl -O http://yum.postgresql.org/9.3/redhat/rhel-6-i386/pgdg-centos93-9.3-1.noarch.rpm
  rpm -ivh pgdg-centos93-9.3-1.noarch.rpm
  yum groupinstall -y "PostgreSQL Database Server 9.3 PGDG"
  yum install -y postgresql93-devel phpPgAdmin
  service postgresql-9.3 initdb
  su -s /bin/bash -c "sed -i 's/ident$/md5/g' /var/lib/pgsql/9.3/data/pg_hba.conf" postgres

  service postgresql-9.3 start
  chkconfig postgresql-9.3 on
  echo 'export PATH=$PATH:/usr/pgsql-9.3/bin/' > /etc/profile.d/pgsql-9.3.sh
else

  yum install -y postgresql93* phpPgAdmin

  service postgresql93 initdb
  su -s /bin/bash -c "sed -i 's/ident$/md5/g' /var/lib/pgsql93/data/pg_hba.conf" postgres

  service postgresql93 start
  chkconfig postgresql93 on
  echo 'export PATH=$PATH:/usr/lib64/pgsql93/bin/' > /etc/profile.d/pgsql-93.sh
fi


su -s /bin/bash -c "printf '12345678\n12345678\n' | createuser -d -R -S -P sample_app" postgres

# RVM & Ruby
su -s /bin/bash -c 'curl -L https://get.rvm.io | bash && \
  echo "install: --no-rdoc --no-ri" > ~/.gemrc && \
  echo "update:  --no-rdoc --no-ri" >> ~/.gemrc && \
  echo "source ~/.profile" >> ~/.bash_profile'\
    sample_app
su -s /bin/bash -c 'PATH=$PATH:$HOME/.rvm/bin rvm install 2.0.0' sample_app

# Repository and gems
su -s /bin/bash --login -c '[[ -d "~/public_html/sample_app" ]] || git clone https://github.com/carmelocuenca/sample_app_rails_4.git ~/public_html/sample_app' \
  sample_app
su -s /bin/bash --login -c 'cp ~/public_html/sample_app/config/database.yml.postgres ~/public_html/sample_app/config/database.yml' \
  sample_app

su -s /bin/bash --login -c 'cd ~/public_html/sample_app && gem install bundler' \
  sample_app

su -s /bin/bash --login -c 'cd ~/public_html/sample_app && \
  bundle install && rake db:create RAILS_ENV=development && rake db:migrate RAILS_ENV=development && rake db:populate RAILS_ENV=development' \
  sample_app

su -s /bin/bash --login -c 'cd ~/public_html/sample_app && rvm wrapper $(rvm current) bootup puma' sample_app

# Virtual hosts
cat > '/etc/httpd/conf.d/sample_app.conf' <<-"TEXT"
NameVirtualHost *:80

<VirtualHost *:80>
  ServerAdmin webmaster@sample_app.example.com
  DocumentRoot /home/sample_app/public_html/sample_app
  ServerName sample_app.example.com
  ErrorLog logs/sample_app.example.com-error_log
  CustomLog logs/sample_app.example.com-access_log common
  RewriteEngine On
  # Redirect all non-static requests to puma
  RewriteCond %{DOCUMENT_ROOT}/%{REQUEST_FILENAME} !-f
  RewriteRule ^/(.*)$ http://127.0.0.1:9292%{REQUEST_URI} [P,QSA,L]
</VirtualHost>
TEXT
/etc/init.d/httpd restart

# DAEMONS
cat > '/etc/init.d/puma_sample_app' <<-"TEXT"
#/bin/bash
### BEGIN INIT INFO
# Provides: puma
# Required-Start: $remote_fs $syslog'
# Required-Stop: '$remote_fs $syslog'
# Default-Start: 2 3 4 5
# Default-Stop: 0 1 6
# Short-Description: Puma webserver
# Description: Puma webserver for sample_app
### END INIT INFO
PUMA=/home/sample_app/.rvm/bin/bootup_puma
PUMA_ARGS="-p 9292 -C /home/sample_app/public_html/sample_app/config/puma.rb"
KILL=/bin/kill
PID=/home/sample_app/public_html/sample_app/tmp/pids/puma.pid

sig() {
  test -s "$PID" && kill -$1 `cat $PID`
}
case "$1" in
start)
  echo "Starting puma $PUMA $PUMA_ARGSs..."
  $PUMA $PUMA_ARGS
;;
stop)
  echo "Stopping puma..."
  sig QUIT && exit 0
  echo >&2 "Not running"
;;
restart)
  $0 stop
  $0 start
;;
status)
;;
*)
  echo "Usage: $0 {start|stop|restart|status}"
esac
TEXT
chmod +x /etc/init.d/puma_sample_app
chkconfig puma_sample_app on
/etc/init.d/puma_sample_app start
